
# API testing using karate
[Karate](https://github.com/intuit/karate) is an open-source API test automation tool. API tests are written using Behaviour Driven Development (BDD) Gherkin syntax

## Project Setup
- Install [Java 8](https://www.oracle.com/technetwork/java/javase/downloads/jdk8-downloads-2133151.html)

- Install [Maven](https://maven.apache.org/install.html)


## Execute Scripts

```
mvn clean test -Dtest=KarateJunit
```

#### command to run specific feature file

For example, to run validatePet_positiveCases feature file with tag "pet" in Environment "Acceptance"

```
mvn clean test -Dtest=KarateJunit -Dcucumber.options="--tags @pet" -DargLine='-Dkarate.env=Acceptance'
```

## Test Reports

folder target -> cucumber-html-reports -> overview-features.html

![TestReport](.\screenshot\Test_Report.jpg)

**Scenarios Tested**

- Feature files added for testing positive and negative scenarios of Pet, Petstore Orders, User API's.

- Feature files added to validate response of each resource.


**Bug Found**

- Issue with error code 400. Response is successful with invalid inputs. 

