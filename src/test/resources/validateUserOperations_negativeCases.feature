@useroperationNegative
Feature: Verification of User operations API negative scenarios

  Background: 
    * url url
    * def username = "Oopo"

  Scenario: Verify error codes for user operations
    
    #User not found 
    Given path 'user/'+username
    And header Content-Type = 'application/json'
    When method GET
    Then status 404
    And match $ contains {"code": 1,"type": "error","message": "User not found"}
    
    Given path 'user/'+username
    And header Content-Type = 'application/json'
    When method DELETE
    Then status 404