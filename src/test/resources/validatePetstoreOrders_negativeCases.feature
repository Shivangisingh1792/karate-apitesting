@petOrdersNegative
Feature: Verification of petstore orders API negative scenarios

  Background: 
    * url url
    * def orderId = "10"

  Scenario: Verify error cases for accessing petstore orders 
    
    #Order not found
    Given path 'store/order/'+orderId
    And header Content-Type = 'application/json'
    When method GET
    Then status 404
    And match response == {"code": 1,"type": "error","message": "Order not found"} 
    
    Given path 'store/order/'+orderId
    And header Content-Type = 'application/json'
    When method DELETE
    Then status 404
    And match response == {"code": 404,"type": "unknown","message": "Order Not Found"}