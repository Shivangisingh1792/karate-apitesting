@pet
Feature: Verification of Pet API positive scenarios 

  Background: 
    * url url

  Scenario: Verify pet access
   
		#Add a new pet to the store
		Given path 'pet'
    And header Content-Type = 'application/json'
    And request {"id": 25,"category": {  "id": 1,  "name": "Pug"},"name": "doggie","photoUrls": [  "www.doggie.com"],"tags": [  {    "id": 2,    "name": "tag2"  }],"status": "available"}
    When method POST
    Then status 200
    And match response == {"id": 25,"category": {  "id": 1,  "name": "Pug"},"name": "doggie","photoUrls": [  "www.doggie.com"],"tags": [  {    "id": 2,    "name": "tag2"  }],"status": "available"}
    * def petId = response.id
    * print karate.pretty(petId)
    
    #Upload image of pet
    Given path 'pet/'+petId+'/uploadImage'
    And header Content-Type = 'multipart/form-data'
    And multipart field file = read('classpath:dog.jpg')
    And multipart field additionalMetadata = 'Dog Pug'
    When method post
    Then status 200
    And match response == {"code":200,"type":"unknown","message":"additionalMetadata: Dog Pug\nFile uploaded to ./file, 48431 bytes"}
    
    #Update an existing pet    
    Given path 'pet'
    And header Content-Type = 'application/json'
    And request {"id": 25,"category": {  "id": 2,  "name": "Bulldog"},"name": "doggie","photoUrls": [  "www.doggie.com"],"tags": [  {    "id": 2,    "name": "tag2"  }],"status": "available"}
    When method PUT
    Then status 200
    And match response == {"id": 25,"category": {  "id": 2,  "name": "Bulldog"},"name": "doggie","photoUrls": [  "www.doggie.com"],"tags": [  {    "id": 2,    "name": "tag2"  }],"status": "available"}

    #Find pet by Id
    Given path 'pet/'+petId
    And header Content-Type = 'application/json'
    When method GET
    Then status 200
    And match response == {"id": 25,"category": {  "id": 2,  "name": "Bulldog"},"name": "doggie","photoUrls": [  "www.doggie.com"],"tags": [  {    "id": 2,    "name": "tag2"  }],"status": "available"}
    
    #Updates a pet in the store with form data
		Given path 'pet/'+petId
    And header Content-Type = 'application/json'
    And form field name = 'Sam'
		And form field status = 'available'
    When method POST
    Then status 200
    And match response == {"code":200,"type":"unknown","message":"25"}
       		
    #Deletes a pet
    Given path 'pet/'+petId
    And header Content-Type = 'application/json'
    When method DELETE
    Then status 200
    And match response == {"code": 200,"type": "unknown","message": "25"}
    
    #Find pet by status
    Given path 'pet/findByStatus'
    And param status = 'available'
    And header Content-Type = 'application/json'
    When method GET
    Then status 200
    
    

