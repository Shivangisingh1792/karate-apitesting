@user
Feature: Verification of User operations API positive scenarios 

  Background: 
    * url url
    * def username = "JohnL"
    * print karate.pretty(username)

  Scenario: Verify user operations
   
		#Create user
		Given path 'user'
    And header Content-Type = 'application/json'
    And request {"id": 100,"username": "JohnL","firstName": "John","lastName": "light","email": "John.light@abc.com","password": "Pass@123","phone": "+31612345678","userStatus": 0}
    When method POST
    Then status 200
    And match response == {"code": 200,"type": "unknown","message": "100"}
    
    #Update user
    Given path 'user/'+username
    And header Content-Type = 'application/json'
    And request {"id": 100,"username": "JohnL","firstName": "John","lastName": "light","email": "light.John@abc.com","password": "Pass@123","phone": "+31612345678","userStatus": 0}
    When method PUT
    Then status 200
    And match response == {"code": 200,"type": "unknown","message": "100"}
    
    #Retreive user by user mane
    Given path 'user/'+username
    And header Content-Type = 'application/json'
    When method GET
    Then status 200
    And match response == {"id": 100,"username": "JohnL","firstName": "John","lastName": "light","email": "light.John@abc.com","password": "Pass@123","phone": "+31612345678","userStatus": 0}
    
    #Logs user into the system
    Given path 'user/login'
    And param username = 'JohnL'
    And param password = 'Pass@123'
    And header Content-Type = 'application/json'
    When method GET
    Then status 200
    And match response == {"code": 200,"type": "unknown","message": #string}
        
    #Logs out current logged in user session
    Given path 'user/logout'
    And header Content-Type = 'application/json'
    When method GET
    Then status 200
    And match response == {"code": 200,"type": "unknown","message": "ok"}
       
    #Delete user
    Given path 'user/'+username
    And header Content-Type = 'application/json'
    When method DELETE
    Then status 200
    And match response == {"code": 200,"type": "unknown","message": "JohnL"}
    
    #Creates list of users with given input array
    Given path 'user/createWithArray'
    And header Content-Type = 'application/json'
    And request [{"id": 101,"username": "Mark@123","firstName": "Mark","lastName": "light","email": "Mark.light@abc.com","password": "Pass@123","phone": "+31612345123","userStatus": 0},{"id": 102,"username": "Erick@123","firstName": "Erick","lastName": "light","email": "Erick.light@abc.com","password": "Pass@123","phone": "+31612345679","userStatus": 0}]
    When method POST
    Then status 200
    And match response == {"code": 200,"type": "unknown","message": "ok"}
    
    #Creates list of users with given input array
    Given path 'user/createWithList'
    And header Content-Type = 'application/json'
    And request [{"id": 101,"username": "Mark@123","firstName": "Mark","lastName": "light","email": "Mark.light@abc.com","password": "Pass@123","phone": "+31612345123","userStatus": 0},{"id": 102,"username": "Erick@123","firstName": "Erick","lastName": "light","email": "Erick.light@abc.com","password": "Pass@123","phone": "+31612345679","userStatus": 0}]
    When method POST
    Then status 200
    And match response == {"code": 200,"type": "unknown","message": "ok"}
    
    
    
    
    
    
    