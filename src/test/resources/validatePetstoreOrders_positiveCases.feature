@petOrders
Feature: Verification of petstore orders API positive scenarios

  Background: 
    * url url

  Scenario: Verify petstore orders access
   
		#Place an order for a pet
		Given path 'store/order'
    And header Content-Type = 'application/json'
    And request {"id": 5,"petId": 9991,"quantity": 0,"shipDate": "2020-10-21T10:18:35.910Z","status": "placed","complete": true}
    When method POST
    Then status 200
    And match response == {"id": 5,"petId": 9991,"quantity": 0,"shipDate": #string,"status": "placed","complete": true}
    * def orderId = response.id
    * print karate.pretty(orderId)
    
    #Find purchase order by ID
    Given path 'store/order/'+orderId
    And header Content-Type = 'application/json'
    When method GET
    Then status 200
    And match response == {"id": 5,"petId": 9991,"quantity": 0,"shipDate": #string,"status": "placed","complete": true}
    
    #Delete purchase order by ID
    Given path 'store/order/'+orderId
    And header Content-Type = 'application/json'
    When method DELETE
    Then status 200
    And match response == {"code": 200,"type": "unknown","message": "5"}
    
    #Returns pet inventories by status
    Given path 'store/inventory'
    And header Content-Type = 'application/json'
    When method GET
    Then status 200
    
    
    
    
    