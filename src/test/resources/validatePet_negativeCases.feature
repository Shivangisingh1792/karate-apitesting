@petNegative
Feature: Verification of Pet API negative scenarios

  Background: 
    * url url
    * def petId = "3131313131"

  Scenario: Verify error codes for accessing Pet
    
    #Pet not found
    Given path 'pet/'+petId
    And header Content-Type = 'application/json'
    When method GET
    Then status 404
    And match response == {"code": 1,"type": "error","message": "Pet not found"}
    
    Given path 'pet/'+petId
    And header Content-Type = 'application/json'
    And form field name = 'Sam'
		And form field status = 'available'
    When method POST
    Then status 404
    And match response == {"code": 404,"type": "unknown","message": "not found"}
    
    Given path 'pet/'+petId
    And header Content-Type = 'application/json'
    When method DELETE
    Then status 404
    
    
